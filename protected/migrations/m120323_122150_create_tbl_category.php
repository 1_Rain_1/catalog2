<?php

class m120323_122150_create_tbl_category extends CDbMigration
	{
	 public function up()
 {
            $this->createTable('tbl_category', array(
                'id'=>'pk',
                'name'=>'Text',
                'description'=>'text',
                'shown'=>'boolean NOT NULL DEFAULT 1'
            ),"ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
 } 


	public function down()
	{
		  $this->dropTable('tbl_category');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}