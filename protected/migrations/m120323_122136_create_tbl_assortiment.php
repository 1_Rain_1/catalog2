<?php

class m120323_122136_create_tbl_assortiment extends CDbMigration
{
	 public function up()
        {
            $this->createTable('tbl_assortiment', array(
                'id'=>'pk',
                'name'=>'Text',
                'description'=>'text',
                'category_id'=>'int',
                'rating'=>'INT',
                'shown'=>'boolean not null default 1'
            ),"ENGINE=InnoDB DEFAULT CHARSET=UTF8 COLLATE=utf8_unicode_ci");
            $this->addForeignKey('category_id', 'tbl_assortiment', 'category_id', 'tbl_category', 'id', 'CASCADE', 'CASCADE');
            
        }



	public function down()
	{
		    $this->dropTable('tbl_assortiment');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}